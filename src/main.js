import { createApp } from 'vue'
import App from './App.vue'
import store from './stores/index.js'
import router from './router.js'
import BaseContainer from './components/ui/BaseContainer.vue';
import PrimeVue from "primevue/config";
import DataView from 'primevue/dataview';
import Dropdown from 'primevue/dropdown';
import DataViewLayoutOptions from "primevue/dataviewlayoutoptions";
import Button from "primevue/button";
import Rating from "primevue/rating";
import InputText from 'primevue/inputtext';
import Tooltip from 'primevue/tooltip';
import Panel from 'primevue/panel';
import Divider from 'primevue/divider';
import Card from 'primevue/card';
import InputNumber from 'primevue/inputnumber';
import OrderList from 'primevue/orderlist';
import ConfirmationService from 'primevue/confirmationservice';
import ConfirmDialog from "primevue/confirmdialog";
import Toast from "primevue/toast";
import ToastService from "primevue/toastservice";
import "primeflex/primeflex.css";
import "primevue/resources/themes/saga-blue/theme.css";
import "primevue/resources/primevue.min.css";
import "primeicons/primeicons.css";

const app = createApp(App)
app.component('base-container', BaseContainer);
app.use(PrimeVue, { ripple: true });
app.component('DataView', DataView);
app.component('Dropdown', Dropdown);
app.component('InputText', InputText);
app.component('Divider', Divider);
app.component('InputNumber', InputNumber);
app.component('Card', Card);
app.component('Panel', Panel);
app.component('DataViewLayoutOptions', DataViewLayoutOptions);
app.component('Button', Button);
app.component('Rating', Rating);
app.component('OrderList', OrderList);
app.directive('Tooltip', Tooltip);
app.use(ToastService);
app.use(ConfirmationService);
app.component("Toast", Toast);
app.component('ConfirmDialog', ConfirmDialog);
app.use(store);
app.use(router);
app.mount('#app')