export default {
    namespaced: true,
    state() {
        return {
            products: [],
            displayproduct: [],
        };
    },
    mutations: {
        setProducts(state, payload) {
            state.products = payload;
        }

    },
    actions: {
        async setProducts(context) {
            const response = await fetch(
                `https://getproducts-92bee.firebaseio.com/products.json`
            );
            const responseData = await response.json();

            if (!response.ok) {
                const error = new Error(responseData.message || 'Failed to fetch!');
                throw error;
            }
            console.log(responseData)
            context.commit('setProducts', responseData);
        },

    },
    getters: {
        products(state) {
            return state.products;
        },



    }
};