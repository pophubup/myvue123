import { createStore } from 'vuex';
import cartModule from './cart/cart.js';
import productModule from './product/product.js';

const store = createStore({
    modules: {
      cart: cartModule,
      product: productModule,
      
    }
  });
  
  export default store;