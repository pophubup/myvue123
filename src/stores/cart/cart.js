export default {
    namespaced: true,
    state() {
        return {
            cart: [],
            order: [],
        };
    },
    mutations: {
        setCart(state, payload) {

            state.cart.push(payload)
        },
        createOrder(state, payload) {
            state.order.push(payload)
        }
    },
    actions: {
        setCart(context, data) {

            const isExisted = context.getters.getCart.filter(x => x.productId == data.productId)
            console.log(isExisted)
            if (isExisted.length == 0) {

                context.commit('setCart', data);
            } else {
                return;
            }
        },
        async createOrder(context, data) {
            const response = await fetch(
                `https://getproducts-92bee.firebaseio.com/orders/${data.productId}.json`, {
                    method: 'PUT',
                    body: JSON.stringify(data)
                }
            );
            if (!response.ok) {
                // error ...
            }

            context.commit('createOrder', data);
        }
    },
    getters: {
        getCart(state) {
            return state.cart;
        },

    }
};