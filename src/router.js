import { createRouter, createWebHistory } from 'vue-router';
import productsList from './pages/products/productList.vue';
import productDetail from './pages/products/productDetail.vue';
import NotFound from './pages/NotFound.vue';
import Cart from './pages/cart/cartList.vue'
const router = createRouter({
    history: createWebHistory(),
    routes: [
        { path: '/', redirect: '/products' },
        { path: '/products', component: productsList },
        {
            path: '/products/:id',
            component: productDetail,
            props: true,
        },
        { path: '/cart', component: Cart },
        //   { path: '/requests', component: RequestsReceived, meta:{requiresAuth:true}  },
        //   { path: '/auth', component: UserAuth , meta:{requiresUnAuth:true} },
        { path: '/:notFound(.*)', component: NotFound }
    ]
});

export default router;